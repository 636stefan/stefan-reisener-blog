+++
date = "2016-01-09T22:02:31+07:00"
description = ""
keywords = []
title = "about"

+++

Hallo,

mein Name ist Stefan Reisener, ich lebe in Surabaya/Indonesien und ich verdiene mein Geld als Web-Developer.

In diesem Blog schreibe ich vor allem über Tools, die mir die Arbeit erleichtern oder die ich in irgendeiner Weise nützlich finde.

Aktuell benutze ich ein Dell Latitude E6410, was mir trotz seines Alters und seiner Klobigkeit inzwischen extrem ans Herz gewachsen ist.

Neben Windows nutze ich vor allem Linux. Mein liebster Window-Manager ist i3, aber auch in Gnome und Unity fühle ich mich zuhause.

&nbsp;

Wenn ich meine Zeit mal nicht vor dem Gerät verbringe fotografiere ich oder erkunde mit meiner Frau die Schönheit Indonesiens. Außerdem koche ich gerne und schreibe unter <a href="http://reisener.asia">Reisener.asia</a> Briefe an meine Enkelkinder.
