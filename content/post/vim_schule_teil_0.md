+++
date = "2016-01-09T22:48:38+07:00"
description = ""
keywords = []
title = "Vim-Schule Teil 0"

+++
Sublime ist eigentlich einer der besten Editoren, die ich kenne. Schnell, durchdacht und stabil. Doch seit etwa zwei Wochen habe ich massive Probleme.   
1. Da ich ein US-Keyboard Layout benutze und auf Umlaute angewiesen bin, verwende ich das "US-International with dead keys layout". Konkret bedeutet das, dass ich zum Beispiel um ein Anführungszeichen zu tippen, die entsprechende Taste drücken muss und dann die Leertaste. Sublime wartet neuerdings nicht mehr auf die Leertaste und erzeugt ein curly Quotationmark, welches sich minimal vom klassischen englischen Anführungszeichen (¨ vs. ") unterscheidet. Eigentlich kein großes Problem, doch dummerweise erzeugt es in den Programmiersprachen, mit denen ich arbeite, Fehlermeldungen oder wird ignoriert. Dieses Problem tritt nur in Sublime auf. In einigen Foren scheint der Bug sogar bekannt zu sein - eine Lösung hat aber niemand.  
2. Packagecontrol.io ist neuerdings öfter offline. An sich kein Problem von Sublime doch Packagecontrol.io hat ein quasi-Monopol auf Packagemanager in Sublime. Auch hier ist das Problem bekannt und der Entwickler hinter dem Projekt ist ziemlich kommunikativ, doch die Lösung lies zu lange auf sich warten.
Und so kommt es, dass ich nun seit einer Woche Vim produktiv einsetze.     
Ich werde nun im folgenden immer wieder von meiner Nutzung berichten und Tipps zum besten geben.
